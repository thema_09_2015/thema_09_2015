#!/usr/bin/python3

'''
Name: db_communicatie.py
Author: Astrid Blaauw & Wout van Helvoirt
Date: October 1th, 2015

Usage:
An module that is able to connect to an mysql database, retrieve data and close
the connection.
It uses an '.my.cnf' configfile as default to connect to the database. This file
should be located in your home directory. For example '/homes/<username>/.my.cnf'

Options available:
    - conf: Should the configfile be used? (default True)

    Optional:
    - group: Groupname (default 'Thema09')
    - host: Hostname (default None)
    - port: Portname (default None)
    - user: Username (default None)
    - db: Database (default None)

    If one of the optional variables above is given, the corresponding variable
    from the configfile will be ignored.

    Groupname has a default value for this theme but can be changed for other
    projects. Within configfile the string between the [] is the groupname.

Password will be used from the configfile. If not available, a prompt will enable
the user to fill in their password.
'''

# Import modules neccesary for program
import getpass
import configparser
import mysql.connector
import sys


class Singleton(object):

  _instances = {}
  _instantiated = False

  def __new__(class_, *args, **kwargs):

    if class_ not in class_._instances:
        class_._instances[class_] = super(Singleton, class_).__new__(class_, *args, **kwargs)
    return class_._instances[class_]


# Class containing function for communication with database
class DBCommunicate(Singleton):
    '''
    This module communicates with the MySQL database and should be imported for
    easy communication.
    '''

    def __init__(self, conf=True, group="Thema09", host=None, port=None, user=None, db=None):

        if not self._instantiated:
            self._instantiated = True
            self.group = group

            # Gather configfile data
            if conf is True:
                parse = self.configfile_parser()

                # Check input variables
                if host is None:
                    host = parse["host"]
                if port is None:
                    port = int(parse["port"])
                if user is None:
                    user = parse["user"]
                if db is None:
                    db = parse["database"]
                if parse["password"]:
                    self.passw = parse["password"]

            # Create variables
            self.host = host
            self.port = port
            self.user = user
            self.db = db
            if self.passw is None:
                self.passw = self.retrieve_password()

            self.conn = None
            self.cur = None

    def configfile_parser(self):
        '''
        This function parses the configfile that is located at the home
        directory of the user
        Input: none
        Output: Dict containing info from given configfile
        '''

        try:
            # Try to open config file given by user
            conf_path = "/homes/" + getpass.getuser() + "/.my.cnf"
            #conf_path = "database/.my.cnf"
            fp = open(conf_path, "r")

            # Parse the given configfile
            config = configparser.ConfigParser()
            config.read_file(fp)
            parsed_info = dict(config.items(self.group))

            return parsed_info

        except (OSError, IOError):
            print("\nError: Config file wasn't found at '" + conf_path + "'")

    def retrieve_password(self):
        '''
        This function retrieves the user password in a secure way
        Input: None
        Output: Password in string format
        '''

        # Get password from user
        passw = getpass.getpass("Please enter password for given information: ")
        return passw

    def connect(self):
        '''
        This function connects to the MySQL database
        Input: None
        Output: None
        '''

        # Try opening database connection
        try:
            self.conn = mysql.connector.connect(host=self.host, port=self.port,
                                                user=self.user, passwd=self.passw, db=self.db)
            self.cur = self.conn.cursor(buffered=True)

        # When mysql error, give error message
        except mysql.connector.Error:
            print("\nMySQL.connector Error: Please use valid login information")
            sys.exit()

    def disconnect(self):
        '''
        This function diconnects from the MySQL database
        Input: None
        Output: None
        '''

        # Close connection and commit data
        self.cur.close()
        self.conn.commit()
        self.conn.close()


def main():
    '''Main function of this module'''
    #print(__doc__)

    h = DBCommunicate()
    h.connect()
    print(h.cur)
    s = DBCommunicate()

    print(s.cur)
    print(h.cur)

if __name__ == '__main__':
    main()
