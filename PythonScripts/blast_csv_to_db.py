#!/usr/bin/env python3
"""
Author: Harm Brugge
Date: 3/10/'15

Script to instert csv output from blast to the database

CSV file must be in this format:
hit_accesion    fasta_header_targer_seq evalue  hit_length  identity    gaps

!! Pas op bij een psi blast zijn subject en query andersom ten opzichte van een blastn of blastp.
Houd hier dus rekening mee met je outfmt!

Usage:
    -i      input file (csv, tab-seperated)
    -q      blast query (will be inserted in db)
    -d      description (will be inserted in db)

"""
import argparse
import os

from beans.annotation import Annotation
from database.annotation_dao import AnnotationDAO
from database.experiment_dao import ExperimentDAO
from database.db_communicatie import DBCommunicate


def get_args():
    """
    Retrieves commandline arguments
    :return: commandline arguments
    """
    description = "Description: Insert csv output from blast to database"
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description=description)

    parser.add_argument('--version', action='version', version='%(prog)s 0.7')
    parser.add_argument('-i', help='input file', type=str)
    parser.add_argument('-q', help='used blast query', type=str)
    parser.add_argument('-d', help='description', type=str)
    parser.add_argument('-e', help='experiment', type=str)

    args = parser.parse_args()

    if not args.i:
        parser.error('[ERROR] No input file. Define one with -i <path/to/file>')
    if not args.q:
        parser.error('[ERROR] No query specified. Define one with -q \'complte blastn query\'')
    if not args.d:
        parser.error('[ERROR] No description specified. Define one with -d \'description\'')

    return args


def parse_file(file, args):
    """
    CSV file must comply to requested format
    :param file: File object
    :return: List of Annotation objects
    """
    annotations = []

    for line in file.readlines():

        # file is tab seperated
        annot = line.split('\t')
        hit_accession = annot[0]
        # header = lcl|contig_id
        contig = annot[1].split("|")
        contig_id = contig[1]

        evalue = annot[2]
        lenght = annot[3]
        ident = annot[4]
        gaps = annot[5]

        if len(contig) > 2:
            reading_frame = contig[2]
            annotations.append(Annotation(None, contig_id, None, evalue, lenght,
                                          ident, gaps, hit_accession, reading_frame))
        else:
            annotations.append(Annotation(None, contig_id, None, evalue, lenght,
                                          ident, gaps, hit_accession, None))

    return annotations


def main():
    args = get_args()

    if not os.path.exists(args.i):
        print('File {0} not found'.format(args.i))
        exit(0)

    annotations = []
    try:
        file = open(args.i, 'r')
        annotations = parse_file(file, args)
    except OSError:
        print('Error reading file {0}'.format(args.i))
        exit(0)

    DBCommunicate().connect()
    annotation_dao = AnnotationDAO()
    experiment_dao = ExperimentDAO()

    if args.e:
        experiment_id = args.e
    else:
        # function returns the inserted id
        experiment_id = experiment_dao.add_experiment(args.q, args.d)

    for annotation in annotations:
        annotation.experiment_id = experiment_id
        annotation_dao.add_annotation(annotation)

    DBCommunicate().disconnect()

if __name__ == '__main__':
    main()
