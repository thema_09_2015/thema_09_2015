#!/usr/bin/env python3
"""
Author's: Olivier Bakker
Usage:
    --version gets the version
    -h, --help print help
    -i      input folder
    -o      output file
    -t      CSV header Y/N
    -d      delimmiter
    -f      generate fasta

The input folder must be the same as in the commons folder.
You can (probarbly) also just specify the commons path as the input

"""

import os
import argparse
import time
import re


def handler(input_folder, output_file, delimmiter, header, make_fasta):
    """
    This function makes the CSV file out of the files in the input folder.
    It does not have error handeling at all because it is pretty specifick,
    if you want to add it feel free :)
    @:param input_folder The input folder
    @:param output_file The output CSV file
    @:param header Specify's if the CSV file has a header or not
    @:param delimmiter the CSV delimmiter
    @:param make_fasta boolean if a fasta file should be made
    @:return void
    """

    # List containing the lines to be written to csv a csv file
    node_list = []
    # List containing the lines to be written to a fasta file
    fasta_list = []
    # Counter to store the line number
    counter = 0

    # makes the csv header
    if header:
        node_list.append("id{0}folder{0}node{0}length{0}cov{0}sequence".format(delimmiter))

    for folder in sorted(os.listdir(input_folder)):
        # makes the path
        cur_folder = os.path.join(input_folder, folder)

        # opens the file
        cur_fa_file = open(os.path.join(cur_folder, os.listdir(cur_folder)[0]), "r")

        # read and parse the file using regex
        for line in cur_fa_file:
            fasta_header = re.search(">NODE_(\d*)_length_(\d*)_cov_(\d*\.\d*)\n", line)
            if fasta_header:
                counter += 1
                node_list.append("\n" + str(counter) + delimmiter + folder[3] + delimmiter + fasta_header.group(1) +
                                      delimmiter + fasta_header.group(2) + delimmiter + fasta_header.group(3)
                                      + delimmiter)
                if make_fasta:
                    fasta_list.append(">lcl|"+str(counter)+"\n")

            else:
                if make_fasta:
                    fasta_list.append(line)
                node_list.append(line.strip("\n"))

        cur_fa_file.close()

    # If make_fasta is true a fasta file will be made
    if make_fasta:
        output_file_2 = open("FastaToBlast.fa", "w")
        output_file_2.write("".join(fasta_list))
        output_file_2.close()

    # Write node_list to csv file
    output_file = open(output_file, "w")
    output_file.write("".join(node_list))
    output_file.close()


def get_args():
    '''
    Parse the cl arguments
    :return: Argument object containing arguments
    '''

    description_string = "Author's: Olivier Bakker\n" \
                         "Email: o.b.bakker@st.hanze.nl\n" \
                         "Description: This program creates a csv file for importation into the initial database."
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description=description_string)

    parser.add_argument('--version', action='version', version='%(prog)s 0.7')
    parser.add_argument('-i', help='input folder', type=str)
    parser.add_argument('-o', help='output file', type=str)
    parser.add_argument('-d', help='delimmiter', type=str, default=';')
    parser.add_argument('-t', help='CSV header', action="store_true", default=False)
    parser.add_argument('-f', help='genarate fasta file', action="store_true", default=False)

    args = parser.parse_args()

    if not args.i:
        parser.error('[ERROR] No input folder defined. Define one with -i <path/to/folder>')

    return args


def main():
    '''
    Main function which does a bit of printing and calls the handler which runs the logic.
    :return: void
    '''

    args = get_args()

    print('+', '-'*76, '+')
    print('|', 'DatasetToCSV'.center(76), '|')
    print('+', '-'*76, '+')

    if os.path.exists(args.o):
        print('[WARNNG] File '+args.o+' exists or is invalid')
        warning = input('Do you want to continue? y/n: ')
        print(type(warning))
        if warning.lower() == 'n':
            exit(0)

    main_start_time = time.clock()
    handler(input_folder=args.i,
            output_file=args.o,
            delimmiter=args.d,
            header=args.t,
            make_fasta=args.f)

    print('-'*80)
    print('[Total time elapsed] ', round((time.clock()-main_start_time), 3))

if __name__ == '__main__':
    main()