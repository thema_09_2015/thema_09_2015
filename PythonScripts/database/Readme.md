### <name>_dao.py
Dit zijn dao's ofwel database acces objects. Voor elke tabel (databehoefte) is het de bedoeling dat die een dao heeft.
 Een dao bevat alle query's en/of calls naar stored procedures.

### db_communicatie.py
Dit is de module dit de database acces handeled. Deze is aangepast naar een singleton model om zo het programeren
te vereenvoudigen en het openen van meerdere connecties te voorkomen.