## Dit is de map voor python scripts##

##DatasetToCSV.py##
Een programma om het orginele dataset om te vormen naar een CSV file (voor de meeste mensen wss niet interessant)

##DBCommunicate.py##
Wanneer je het .my.cnf bestand in je eigen home directory hebt staan maakt deze basis module connectie met MySQL. Standaard wordt er gebruikt gemaakt van de gegevens uit het config bestand.
Deze functionaliteit kan je ook bij de scripts gebruiken die de resultaten naar de individuele database gaat schrijven.

Opties beschikbaar:

* conf: Moet het config bestand gebruikt worden? \(standaard True\)

Optioneel:

* group: Groupnaam \(standaard 'Thema09'\)
* host: Hostnaam \(standaard None\)
* port: Portnaam \(standaard None\)
* user: Usernaam \(standaard None\)
* db: Database \(standaard None\)

Wanneer een van de bovenstaande optionele waardes gegeven wordt, dan wordt de bijbehorende waarde uit het config bestand niet gebruikt.
Wachtwoord kan nu wel worden meegegeven in config file, zodat de module opzich zelf staand kan functioneren. Wanneer niet meegegeven zal er veilig gevraagd worden naar het wachtwoord.

Minimale opmaak van .my.cnf bestand is:

```sql
[Thema09]
host        = mysql.bin
port        = 3306
user        = <gebruikers naam>
database    = Thema09
password	= <wachtwoord gebruiker (optioneel, maar sterk aan te raden)>

local-infile=1
```

Alles onder de group \(tot een eventuele nieuwe group\) hoort bij dezelfde group. Je kan je eigen .my.cnf info toevoegen aan de .my.cnf opmaak die hierboven staat minst je maar verschillende group namen gebruikt.

Voorbeeld van meerdere groups gebruiken:

```sql
[client]
host        = mysql.bin
port        = 3306
user        = <gebruikers naam>
database    = <eigen database naam>
password	= <wachtwoord gebruiker (optioneel, maar sterk aan te raden)>

local-infile=1

[Thema09]
host        = mysql.bin
port        = 3306
user        = <gebruikers naam>
database    = Thema09
password	= <wachtwoord gebruiker (optioneel, maar sterk aan te raden)>

local-infile=1
```