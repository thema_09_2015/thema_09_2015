from beans.hyp_protein import HypProtein


class HypProteinCreator():
    '''
    Class used for translating a nucleotide seqeunce
    nto all 6 corrosponding reading frames
    '''

    # Map containing bacterial codons
    CODON_MAP = {'ttt': 'F', 'tct': 'S', 'tat': 'Y', 'tgt': 'C',
                 'ttc': 'F', 'tcc': 'S', 'tac': 'Y', 'tgc': 'C',
                 'tta': 'L', 'tca': 'S', 'taa': '*', 'tga': '*',
                 'ttg': 'L', 'tcg': 'S', 'tag': '*', 'tgg': 'W',
                 'ctt': 'L', 'cct': 'P', 'cat': 'H', 'cgt': 'R',
                 'ctc': 'L', 'ccc': 'P', 'cac': 'H', 'cgc': 'R',
                 'cta': 'L', 'cca': 'P', 'caa': 'Q', 'cga': 'R',
                 'ctg': 'L', 'ccg': 'P', 'cag': 'Q', 'cgg': 'R',
                 'att': 'I', 'act': 'T', 'aat': 'N', 'agt': 'S',
                 'atc': 'I', 'acc': 'T', 'aac': 'N', 'agc': 'S',
                 'ata': 'I', 'aca': 'T', 'aaa': 'K', 'aga': 'R',
                 'atg': 'M', 'acg': 'T', 'aag': 'K', 'agg': 'R',
                 'gtt': 'V', 'gct': 'A', 'gat': 'D', 'ggt': 'G',
                 'gtc': 'V', 'gcc': 'A', 'gac': 'D', 'ggc': 'G',
                 'gta': 'V', 'gca': 'A', 'gaa': 'E', 'gga': 'G',
                 'gtg': 'V', 'gcg': 'A', 'gag': 'E', 'ggg': 'G'
                 }

    def __init__(self, contigs):
        '''
        Constructor.
        :param contigs: List containing contig bean objects
        :return: void
        '''
        self.contigs = contigs

    def make(self):
        '''
        Main function that loops over all contigs
        :return: List of hypothetical protein beans
        '''

        amino_contigs = []

        for contig in self.contigs:
            sequences = self.make_codons(contig.sequence)
            # amino_contigs = [ContigAminoAcid(sequences[x], x, contig) for x in range(0, len(sequences))]

            for x in range(0, 6):
                amino_contigs.append(HypProtein(sequences[x], x+1, contig))

        return amino_contigs

    def make_codons(self, sequence):
        '''
        Translates a single sequence into all 6 reading frames.
        :param sequence: the nucleotide seqeunce input
        :return: The 6 reading frames in list format
        '''
        sequences = []

        sequences += self.make_reading_frames(sequence)

        # Make reverse complement
        revcomp = sequence[::-1].translate(bytes.maketrans(b'atcgn', b'tagcn'))

        sequences += self.make_reading_frames(revcomp)

        return sequences

    def make_reading_frames(self, sequence):
        '''
        Make 3 reading frames in the same strand.
        :param sequence: nucleotide seqeunce
        :return: list of 3 reading frames in the same strand
        '''

        sequences = []
        for n in range(0, 3):
            codon_list = []

            for i in range(0, len(sequence), 3):
                cur_codon = sequence[i+n:i+n+3]
                if len(cur_codon) == 3:
                    codon_list.append(self.translate_codon(cur_codon))
                else:
                    break
            sequences.append(codon_list)

        return sequences

    def translate_codon(self, codon):
        '''
        Translates a codon into the corrosponding aminoacid
        :param codon: the codon
        :return: the amino acid
        '''

        if "n" in codon:
            amino_acid = "X"
        else:
            amino_acid = self.CODON_MAP[codon]

        return amino_acid





