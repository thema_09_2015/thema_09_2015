"""
Author: Harm Brugge
Date: 3/10/'15

Weet niet of dit een goede manier van indelen is
"""


class Annotation:
    """
    Data holder class for annotations
    """
    def __init__(self, hsp_id, contig_id, experiment_id, evalue,
                 align_length, identity, gaps, hit_accession, reading_frame):
        self.hsp_id = hsp_id
        self.contig_id = contig_id
        self.experiment_id = experiment_id
        self.evalue = evalue
        self.align_length = align_length
        self.identity = identity
        self.gaps = gaps
        self.hit_accession = hit_accession
        self.reading_frame = reading_frame

    def __str__(self):
        return self.hit_accession
