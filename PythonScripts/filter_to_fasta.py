#!/usr/bin/env python3

'''
Author: Astrid Blaauw
Date: 13/10/'15

Script to write database stored procedure output to fasta.
Creates fastafile with specific filtered contigs.

Fasta output:
    >lcl|<contig id>
    sequence

Usage:
    -p  minimal evalue cutoff for plant filtering
    -n  minimal evalue cutoff for parasite and algae filtering
    -a  minimal evalue cutoff for animal cell filtering
    -o  output filename
'''

import argparse 
from database.db_communicatie import DBCommunicate


def get_args():
    '''
    Retrieves commandline arguments.
    '''
    description = "Insert fasta output filename and cutoff evalues for personalized contig filtering"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-p", help="Minimal evalue cutoff for plant filtering", type=float)
    parser.add_argument("-n", help="Minimal evalue cutoff for parasite and algae filtering", type=float)
    parser.add_argument("-a", help="Minimal evalue cutoff for animal cell filtering", type=float)
    parser.add_argument("-o", help="Output filename", type=str)
    return parser.parse_args()


def filter_contigs(min_eval_plant, min_eval_parasite_algae, min_eval_animal):
    '''
    Returns filtered contig id´s and sequences in list:
    [[contig_id, contig_sequence], [...], ...] 
    '''

    aaa = DBCommunicate()
    aaa.connect()
    
    exp_parasite_algae = 2
    exp_plant = 3
    exp_animal = 19
    
    results = list()

    aaa.cur.callproc("sp_filter_contigs", (exp_plant, exp_parasite_algae, exp_animal, min_eval_plant, min_eval_parasite_algae, min_eval_animal))
	
    for line in aaa.cur.stored_results():
        for r in line.fetchall():
            results.append([r[0], r[2]])
            
    aaa.disconnect()
    return results
    
    
def write_fasta(results, outputname):
    '''
    Generates fastafile from sql stored procedure result list input.
    Fasta output:
        >lcl|<contig id>
        sequence
    '''
    print("Number of results:", len(results))

    newfasta = open((outputname + ".fa"), "w")
    
    for result in results:
            contig_id = result[0]
            contig_seq = result[1]
            newfasta.write(">lcl|{}\n{}\n".format(contig_id, contig_seq))
    newfasta.close()


def main():
    args = get_args()
    results = filter_contigs(args.p, args.n, args.a)
    write_fasta(results, args.o)

if __name__ == '__main__':
    main()