from database.db_communicatie import DBCommunicate
import sys
import mysql.connector


class AnnotationDAO():

    def __init__(self):
        self.db = DBCommunicate()

    def add_annotation(self, annotation):
        """
        Adds annotation to DB
        The annotation must contain a corresponding experiment key.
        :param annotation: Annotation object
        :return: None
        """
        try:
            add_annotation = ("INSERT INTO Annotations "
                              "(contig_id, experiment_id, e_value, align_length, "
                              "identity, gaps, hit_accession, reading_frame)"
                              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")

            self.db.cur.execute(add_annotation, (annotation.contig_id,
                                annotation.experiment_id,
                                annotation.evalue,
                                annotation.align_length,
                                annotation.identity,
                                annotation.gaps,
                                annotation.hit_accession,
                                annotation.reading_frame))

        except mysql.connector.Error as e:
            print("\nMySQL.connector Error: Annotation can't be added to database")
            print(e.msg)
            sys.exit()

    def get_annotations(self, experiment_id, max_e_value):
        """
        Retrieve hsp ID's.
        Input: experiment ID as interger, max evalue as float
        Output: Alle hsp ID's related to given input
        """
        
        try:
            int_id = int(experiment_id)
            float_evalue = float(max_e_value)
            self.db.cur.callproc("sp_get_annotations", (int_id,
                                                        float_evalue))
            hsp_ids = self.db.cur.stored_results()

            return hsp_ids.fetchall()

        except ValueError:
            print("\nValueError: Given experiment id is not an integer or max evalue is not an float")
            sys.exit()

        except mysql.connector.Error:
            print("\nMySQL.connector Error: Given experiment id or max evalue doesn't exist in database")
            sys.exit()

    def delete_experiment(self, experiment_id):
        """
        Remove experiment from Annotations and Experiments table in database
        Input: experiment ID as interger
        Output: None
        """
        
        try:
            int_id = int(experiment_id)
            self.db.cur.callproc("sp_delete_experiment", (int_id))

        except ValueError:
            print("ValueError: Given experiment id is not an integer")
            sys.exit()

        except mysql.connector.Error:
            print("\MySQL.connector Error: Given experiment id doesn't exist in database")
            sys.exit()
