### Welkom mede studenten ###

Dit is de gezamelijke repo voor dit thema. Men zou het erg waarderen als uiteindelijk al het gemaakte gezamelijke werk in deze repo zou verschijnen.
Text files en eventuele afbeeldingen of flowcharts kunnen in de meta folder geplaatst worden.

### Aanbevolen workflow ###
![PipelineProject.png](/Meta/PipelineProject.png)

### Voor mensen die aanpassingen willen maken ###
Overleg dit met het desbetreffende persoon, stuur een message of maak een opmerking bij de desbetreffende commit.

### Planning en deadlines ###
| Taak | Personen | Deadline |
| --------|---------|-------|
|Planten DB maken | Harm| Klaar|
|Database ontwerp|Olivier & Lonneke|Klaar (eventueel aanpassing vereist)|
|Database connector + toevoegen van dataset aan database|Astrid & Wout|Klaar|
|Stored Procedures|Lonneke| voorlopig klaar, tenzij meer mensen met een idee komen|
|Script om database output te parsern naar fasta|Astrid|14/15 oktober|
|Script om dataset om te vormen naar aminozuren| Olivier | 13/14 oktober |
|Filteren plant sequenties|Jeunard & Julius & Stephan|Klaar(moet nog gecontroleerd worden)|
|Filteren op pens fauna|Sander & Henri|Voorlopige resultaten - woensdag 7 oktober 23:59 Eindresultaten - woensdag 14 oktober 23:59|
|Filteren op nematoden/lintwormen|Bas & Elina|Klaar|
|Testen SP's DB connector|Henri & Sander||

### Data in de commons map ###
In de commons map van dit thema staan data zoals de planten database

### Inloggen op database ###
Database: Thema09
Inlog: Je eigen inlog gegevens

### !! PSI Blast ###
Voor commandline psi blast is het outfmt anders ten opzicht van blastp en blastn.
De eerste twee argument zijn omgedraaid. Subject is in een psiblast nml de query:
```bash
-outfmt '6 qacc sseqid evalue length pident gaps'
```

### BlastN ###
Voor commandline blastn gebruiken we outfmt 6 met de volgende paramaters
```bash
-outfmt '6 sseqid qacc evalue length pident gaps'
```
Een blastn query zou er dus zo uit kunnen zien:
```bash
blastn -query filename.fa -db dbname -outfmt '6 sseqid qacc evalue length pident gaps' -max_target_seqs 1
```

### Handig!! ###

In de map misc_scripts, heb ik een scriptje gezet voor het deselecteren en selecteren van input boxes voor de PSI-blast NCBI web-interface.
Super handig want de ui ondersteund dit standaard niet.