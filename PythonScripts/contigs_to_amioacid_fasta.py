#!/usr/bin/env python3
"""
Author's: Olivier Bakker
Usage:
    -i      list of 3 experiment id's
    -e      list of 3 expect value's
    -o      output file
"""

import os
import argparse
import time
from backend.hyp_protein_creator import HypProteinCreator
from database.contig_dao import ContigDAO


def handler(output_file, experiment_id, evalue):
    '''
    Get the database results and write them to a file
    :param output_file: The output file
    :param experiment_id: List of experiment id' s
    :param evalue: List of corrosponding evalues
    :return: void
    '''

    contig_dao = ContigDAO()
    contig_dao.connect()
    contig_list = contig_dao.sp_filter_contigs(experiment_id, evalue)
    contig_dao.disconnect()

    hyp_protein_creator = HypProteinCreator(contig_list)
    hyp_protein_list = hyp_protein_creator.make()

    out_file = open(output_file, "w")
    for hyp_protein in hyp_protein_list:
        header = ">lcl|" + str(hyp_protein.contig_id) + "|" + str(hyp_protein.hyp_protein_id) + "\n"
        content = hyp_protein.hyp_protein_sequence_string + "\n"
        out_file.write(header + content)

    out_file.close()


def get_args():
    '''
    Parse the cl arguments
    :return: Argument object containing arguments
    '''

    description_string = "Author's: Olivier Bakker\n" \
                         "Email: o.b.bakker@st.hanze.nl\n" \
                         "Description: This program creates a fasta file with amino acids seqeunces."
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description=description_string)

    parser.add_argument('--version', action='version', version='%(prog)s 0.7')
    parser.add_argument('-i', help='experiment id', type=int, nargs=3)
    parser.add_argument('-o', help='output file', type=str)
    parser.add_argument('-e', help='evalue', type=float, nargs=3)

    args = parser.parse_args()

    if not args.o:
        parser.error('[ERROR] No output file defined. Define one with -o <path/to/folder/file.extention>')

    return args


def main():
    '''
    Main function which does a bit of printing and calls the handler which runs the logic.
    :return: void
    '''
    args = get_args()

    print('+', '-'*76, '+')
    print('|', 'Hypothetical protein creator for PSI blast'.center(76), '|')
    print('+', '-'*76, '+')

    if os.path.exists(args.o):
        print('[WARNNG] File '+args.o+' exists or is invalid')
        warning = input('Do you want to continue? y/n: ')
        print(type(warning))
        if warning.lower() == 'n':
            exit(0)

    main_start_time = time.clock()
    handler(output_file=args.o,
             experiment_id=args.i,
             evalue=args.e)

    print('-'*80)
    print('[Total time elapsed] ', round((time.clock()-main_start_time), 3))

if __name__ == '__main__':
    main()
