#changes were made to the HypProteinCreator script
# instead of using a database this script will translate a fastafile


class HypProteinCreator():

    codon_map = {'ttt': 'F', 'tct': 'S', 'tat': 'Y', 'tgt': 'C',
                 'ttc': 'F', 'tcc': 'S', 'tac': 'Y', 'tgc': 'C',
                 'tta': 'L', 'tca': 'S', 'taa': '*', 'tga': '*',
                 'ttg': 'L', 'tcg': 'S', 'tag': '*', 'tgg': 'W',
                 'ctt': 'L', 'cct': 'P', 'cat': 'H', 'cgt': 'R',
                 'ctc': 'L', 'ccc': 'P', 'cac': 'H', 'cgc': 'R',
                 'cta': 'L', 'cca': 'P', 'caa': 'Q', 'cga': 'R',
                 'ctg': 'L', 'ccg': 'P', 'cag': 'Q', 'cgg': 'R',
                 'att': 'I', 'act': 'T', 'aat': 'N', 'agt': 'S',
                 'atc': 'I', 'acc': 'T', 'aac': 'N', 'agc': 'S',
                 'ata': 'I', 'aca': 'T', 'aaa': 'K', 'aga': 'R',
                 'atg': 'M', 'acg': 'T', 'aag': 'K', 'agg': 'R',
                 'gtt': 'V', 'gct': 'A', 'gat': 'D', 'ggt': 'G',
                 'gtc': 'V', 'gcc': 'A', 'gac': 'D', 'ggc': 'G',
                 'gta': 'V', 'gca': 'A', 'gaa': 'E', 'gga': 'G',
                 'gtg': 'V', 'gcg': 'A', 'gag': 'E', 'ggg': 'G'
                 }

    def __init__(self,File):
        self.File = File

    def make(self):

        amino_contigs = []
        for i in open(self.File):
            i = i.replace('\n','')
            if i.startswith('>'):
                
                amino_contigs.append(i)
            else:
                
                sequences = self.make_codons(i.lower())	
       #for contig in self.contigs:
           #2sequences = self.make_codons(contig.sequence)
            #sequences = self.make_codons(contig)
            # amino_contigs = [ContigAminoAcid(sequences[x], x, contig) for x in range(0, len(sequences))]

            #for x in range(0, 6):
                seq = ''.join(map(str, sequences))
                amino_contigs.append(seq)

        return amino_contigs

    def make_codons(self, sequence):

        sequences = []
        for n in range(0, 3):
            codon_list = []
            for i in range(0, len(sequence), 3):
                cur_codon = sequence[i+n:i+n+3]

                if len(cur_codon) == 3:
                    codon_list.append(self.translate_codon(cur_codon))
                else:
                    break
            sequences.append(''.join(codon_list))

        sequence = sequence[::-1]
        for n in range(0, 3):
            codon_list = []

            for i in range(0, len(sequence), 3):
                cur_codon = sequence[i+n:i+n+3]
                if len(cur_codon) == 3:
                    codon_list.append(self.translate_codon(cur_codon))
                else:
                    break
            sequences.append(''.join(codon_list))
           
        return sequences

    def translate_codon(self, codon):

        if "n" in codon:
            amino_acid = "X"
        else:
            amino_acid = self.codon_map[codon]

        return amino_acid
def main():
	try:
		hyp = HypProteinCreator(input("In_file: "))
		newfile = open(input("out_file: "),'w')

		for hyp in hyp.make():
			if hyp.startswith('>'):
				header = hyp
				
			else:
				content = hyp
				newfile.writelines(header+'\n'+content+'\n')
	except NameError and FileNotFoundError:
		print("invalid files")

			
	

if __name__ == "__main__":
	main()

