#!/usr/bin/env python3
from database.db_communicatie import DBCommunicate
from beans.contig import Contig


class ContigDAO(DBCommunicate):
    '''
    Database acces object used for getting contig information out of the database.
    Inherrits from DBCommunicatie singleton.
    '''

    def sp_filter_contigs(self, experiment_id, max_e_value):
        '''
        Calls the sp_filter_contigs stored procedure.
        :param experiment_id: List of 3 experiment id's
        :param max_e_value: List of 3 corrosponding evalue's
        :return: List of contig beans.
        '''

        contig_list = []

        self.cur.callproc('sp_filter_contigs', (experiment_id[0],
                                                experiment_id[1],
                                                experiment_id[2],
                                                max_e_value[0],
                                                max_e_value[1],
                                                max_e_value[2]))

        for line in self.cur.stored_results():
            for result in line.fetchall():
                contig_list.append(Contig(result[0], result[1], result[2]))

        return contig_list


