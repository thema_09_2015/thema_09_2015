__author__ = 'obbakker'
from beans.contig import Contig


class HypProtein(Contig):
    '''
    Hypothetical protein bean which inherrits from contig.
    '''

    def __init__(self, amino_acid_sequences, amino_contig_id, contig):
        super(HypProtein, self).__init__(contig.contig_id, contig.fermenter_id, contig.sequence)
        self.hyp_protein_id = amino_contig_id
        self.hyp_protein_sequence_list = amino_acid_sequences
        self.hyp_protein_sequence_string = "".join(self.hyp_protein_sequence_list)
