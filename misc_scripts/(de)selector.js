/*
    Selector and deselector for input boxex
*/

var checkboxes;

// Deselect all input boxes with name 'checked_GI'
function deselect() {
    checkboxes = document.getElementsByTagName('input');
    
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].name == 'checked_GI') {
            checkboxes[i].checked = false;
        }
    }
}

//Select nr of input boxes with name 'checked_GI'
function select(nr) {
    var count = 0;
    
    checkboxes = document.getElementsByTagName('input');

    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].name == 'checked_GI') {
            count++;
            checkboxes[i].checked = true;
            if (count > nr) {
                break;
            }
        }
    }
}
