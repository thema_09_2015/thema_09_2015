#!/usr/bin/env python3
from database.db_communicatie import DBCommunicate
import sys
import mysql.connector


class ExperimentDAO():

    def __init__(self):
        self.db = DBCommunicate()

    def add_experiment(self, blast_query, description):
        """
        Adds an experiment to DB
        :param blast_query: String
        :param description: String
        :return: Generated primary key
        """

        try:
            add_experiment = ("INSERT INTO Experiments "
                              "(blast_query, omschrijving) "
                              "VALUES (%s, %s)")
            self.db.cur.execute(add_experiment, (blast_query, description))

            return self.db.cur.lastrowid

        except mysql.connector.Error:
            print("\nMySQL.connector Error: Experiment can't be added to database")
            sys.exit()

    def delete_experiment(self, experiment_id):
        """
        Remove experiment from Annotations and Experiments table in database
        Input: experiment ID as interger
        Output: None
        """
        
        try:
            int_id = int(experiment_id)
            self.db.cur.callproc("sp_delete_experiment", (int_id))

        except ValueError:
            print("\nValueError: Given experiment id is not an integer")
            sys.exit()

        except mysql.connnector.Error:
            print("\nMySQL.connector Error: Given experiment id doesn't exist in database")
            sys.exit()
